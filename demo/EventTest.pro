QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    PSEventController.cpp \
    childwidget.cpp \
    data.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    EventType_global.h \
    PSEventController.h \
    Singleton.h \
    childwidget.h \
    data.h \
    widget.h \
    StaticLib1.h

FORMS += \
    widget.ui


LIBS += -L$$PWD/ -lStaticLib1

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
