﻿#ifndef CHILDWIDGET_H
#define CHILDWIDGET_H

#include "data.h"
#include <QDebug>
#include <QWidget>

namespace TestW {

class ChildWidget : public QWidget {
  Q_OBJECT
public:
  ChildWidget(QWidget *parent = nullptr);

public slots:
  void on_psEvent_Test(Core::Data *data) { 
	  qDebug() << data->m_Num; }
};
} // namespace TestW

#endif // CHILDWIDGET_H
