﻿#include "childwidget.h"
#include "PSEventController.h"

namespace TestW {

ChildWidget::ChildWidget(QWidget *parent) : QWidget(parent) {
  subscribe(this, "Test");
}
} // namespace TestW
