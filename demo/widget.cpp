﻿#include "widget.h"
#include "PSEventController.h"
#include "data.h"
#include "ui_widget.h"
#include <QDebug>

using namespace TestW;
Widget::Widget(QWidget *parent)
    : QWidget(parent), ui(new Ui::Widget),
      m_ChildWidget(new ChildWidget(this)) {
  ui->setupUi(this);
  qRegisterMetaType<Core::Data>("Core::Data");
  subscribe(this, "showText");
}

Widget::~Widget() {
  //  PSEventController::clearEvents();
  delete ui;
}

void Widget::in_psEvent_showText(QString msg) {
  ui->label->setText(msg);
  unSubscribe(this, "showText");
}

void Widget::on_pushButton_clicked() {
  auto text = ui->lineEdit->text();
  Core::Data *data = new Core::Data();
  data->m_Num = 10;
  publish("showText", Q_ARG(QString, "123"));
}
