﻿#ifndef WIDGET_H
#define WIDGET_H

#include "childwidget.h"
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

enum EType { ET_Num1 = 0, ET_Test, ET_Main };

class Widget : public QWidget {
  Q_OBJECT

public:
  Widget(QWidget *parent = nullptr);
  ~Widget();

private slots:
  void on_pushButton_clicked();

  void in_psEvent_showText(QString msg);

private:
  Ui::Widget *ui;
  TestW::ChildWidget *m_ChildWidget;

  //    Q_INVOKABLE void on_psEvent_showText(const QString& msg);
};
#endif // WIDGET_H
