﻿#ifndef SINGLETON_H
#define SINGLETON_H
#include <memory>
#include <mutex>

template <typename T> class Singleton {
public:
  Singleton(Singleton &&) = default;
  Singleton &operator=(Singleton &&) = default;
  static T &instance() {
    if (nullptr == instance_) {
      std::lock_guard<std::mutex> latch{mtx_};
      if (nullptr == instance_)
        instance_ = std::make_shared<T>();
    }
    return *instance_;
  }
  static void release() {
    std::lock_guard<std::mutex> latch{mtx_};
    instance_.release();
  }

private:
  Singleton() = default;

private:
  static std::mutex mtx_;
  static std::shared_ptr<T> instance_;
};

template <typename T> std::mutex Singleton<T>::mtx_;
template <typename T> std::shared_ptr<T> Singleton<T>::instance_;

#endif // SINGLETON_H
