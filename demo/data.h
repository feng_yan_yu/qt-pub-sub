﻿#ifndef DATA_H
#define DATA_H

#include <QObject>

namespace Core {

class Data {
public:
  Data();

  int m_Num{1};
  QString m_TestVal;
  int m_Test;
};
} // namespace Core

#endif // DATA_H
